package com.example.coroutinecoding.HW

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PerformnSingletonNetworkRequest (
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun performSingleNetworkRequest() {
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
            } catch (exception: Exception) {
                emptyList<String>()
            }
        }
    }
}