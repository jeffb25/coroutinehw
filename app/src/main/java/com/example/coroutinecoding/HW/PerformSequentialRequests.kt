package com.example.coroutinecoding.HW

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.system.measureTimeMillis

class PerformSequentialRequests(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun a() {
        viewModelScope.launch {
            mockApi.getUsers()
        }
    }
    fun b() {
        viewModelScope.launch {
            mockApi.getCategories()
        }
    }
    suspend fun complex() {
        a()
        b()
    }
}