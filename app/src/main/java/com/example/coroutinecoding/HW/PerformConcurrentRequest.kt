package com.example.coroutinecoding.HW

import kotlinx.coroutines.*


class PerformConcurrentRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun performConcurrentRequest() {
        viewModelScope.launch {
            val one = async { mockApi.getUsers() }
            val two = async { mockApi.getCategories() }
            println("The result is ${one.await() + " " + two.await()}")
        }

    }
}