package com.example.coroutinecoding.HW

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PerformDynamicRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    suspend fun performDynamicRequest(): List<List<List<String>>> {
        return mockApi.getCategories().map {
            it.map { it -> mockApi.getCategoryList(it.toString()) }
        }

    }


}




